<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220213204133 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE positioning DROP CONSTRAINT fk_2b2a70191be62e47');
        // $this->addSql('DROP SEQUENCE mission_id_seq CASCADE');
        // $this->addSql('DROP SEQUENCE positioning_id_seq CASCADE');
        // $this->addSql('DROP SEQUENCE profile_id_seq CASCADE');
        // $this->addSql('DROP SEQUENCE user_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE message_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE status_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ticket_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ticket_message_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE message (id INT NOT NULL, ticket_id INT DEFAULT NULL, id_user BOOLEAN NOT NULL, date_message TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, description VARCHAR(500) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6BD307F700047D2 ON message (ticket_id)');
        $this->addSql('CREATE TABLE status (id INT NOT NULL, name VARCHAR(30) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ticket (id INT NOT NULL, id_user INT NOT NULL, title VARCHAR(30) NOT NULL, date_ticket TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, id_status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ticket_message (id INT NOT NULL, id_ticket BOOLEAN NOT NULL, id_message BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        // $this->addSql('DROP TABLE mission');
        // $this->addSql('DROP TABLE positioning');
        // $this->addSql('DROP TABLE profile');
        // $this->addSql('DROP TABLE "user"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307F700047D2');
        // $this->addSql('DROP SEQUENCE message_id_seq CASCADE');
        // $this->addSql('DROP SEQUENCE status_id_seq CASCADE');
        // $this->addSql('DROP SEQUENCE ticket_id_seq CASCADE');
        // $this->addSql('DROP SEQUENCE ticket_message_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE mission_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE positioning_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE profile_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE mission (id INT NOT NULL, title VARCHAR(255) NOT NULL, description TEXT NOT NULL, id_author INT NOT NULL, publication_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, start_date DATE NOT NULL, end_date DATE DEFAULT NULL, remuneration DOUBLE PRECISION DEFAULT NULL, id_candidate INT DEFAULT NULL, id_status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE positioning (id INT NOT NULL, id_mission_id INT NOT NULL, id_canditate INT NOT NULL, positioning_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, candidacy TEXT NOT NULL, id_status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_2b2a70191be62e47 ON positioning (id_mission_id)');
        $this->addSql('CREATE TABLE profile (id INT NOT NULL, status INT NOT NULL, company_name VARCHAR(255) DEFAULT NULL, level_account INT NOT NULL, siret VARCHAR(10) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, lastname VARCHAR(50) NOT NULL, firstname VARCHAR(50) NOT NULL, token VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_8d93d649e7927c74 ON "user" (email)');
        $this->addSql('ALTER TABLE positioning ADD CONSTRAINT fk_2b2a70191be62e47 FOREIGN KEY (id_mission_id) REFERENCES mission (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        // $this->addSql('DROP TABLE message');
        // $this->addSql('DROP TABLE status');
        // $this->addSql('DROP TABLE ticket');
        // $this->addSql('DROP TABLE ticket_message');
    }
}
