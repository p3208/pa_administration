#!/bin/bash
docker build \
  --target api_platform_php \
  -t $TARGETED_REGISTRY_IMAGE_PHP \
  .
docker build \
  --target api_platform_nginx \
  -t $TARGETED_REGISTRY_IMAGE_SRV \
  .

docker push $TARGETED_REGISTRY_IMAGE_SRV
docker push $TARGETED_REGISTRY_IMAGE_PHP
