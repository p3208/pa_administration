<?php
namespace App\DataPersister;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Ticket;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\Security\Core\Security;

class TicketDataPersister implements DataPersisterInterface
{
    private $entityManager;
    private $JWTEncoder;
    private $security;
    public function __construct(EntityManagerInterface $entityManager, JWTEncoderInterface $JWTEncoder, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->JWTEncoder = $JWTEncoder;
        $this->security = $security;
    }
    public function supports($data): bool
    {
        return $data instanceof Ticket;
    }
    /**
     * @param Ticket $data
     */
    public function persist($data)
    {
        $data->setIdUser($this->JWTEncoder->decode($this->security->getToken()->getCredentials())["id"]);
        $data->setDateTicket(new \DateTime('NOW'));
        $data->setIdStatus(1);
        $data->setTitle($data->getTitle());
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
