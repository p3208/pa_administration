<?php
namespace App\DataPersister;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\Security\Core\Security;

class MessageDataPersister implements DataPersisterInterface
{
    private $entityManager;
    private $JWTEncoder;
    private $security;
    public function __construct(EntityManagerInterface $entityManager, JWTEncoderInterface $JWTEncoder, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->JWTEncoder = $JWTEncoder;
        $this->security = $security;
    }
    public function supports($data): bool
    {
        return $data instanceof Message;
    }
    /**
     * @param Message $data
     */
    public function persist($data)
    {
        $data->setIdUser($this->JWTEncoder->decode($this->security->getToken()->getCredentials())["id"]);
        $data->setDateMessage(new \DateTime('NOW'));
        $data->setDescription($data->getDescription());
        $data->setTicket($data->getTicket());
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
