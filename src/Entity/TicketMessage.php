<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TicketMessageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TicketMessageRepository::class)
 */
class TicketMessage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $idTicket;

    /**
     * @ORM\Column(type="boolean")
     */
    private $idMessage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdTicket(): ?bool
    {
        return $this->idTicket;
    }

    public function setIdTicket(bool $idTicket): self
    {
        $this->idTicket = $idTicket;

        return $this;
    }

    public function getIdMessage(): ?bool
    {
        return $this->idMessage;
    }

    public function setIdMessage(bool $idMessage): self
    {
        $this->idMessage = $idMessage;

        return $this;
    }
}
