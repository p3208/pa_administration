<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TicketRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      collectionOperations={
 *          "GET" = {
 *              "security"="is_granted('ROLE_USER')"
 *          },
 *          "POST" = {
 *              "security"="is_granted('ROLE_USER')"
 *          }
 *     },
 *     itemOperations={
 *     "GET" = {
 *              "security"="is_granted('ROLE_USER')"
 *          },
 *     "PUT" = {
 *              "security"="is_granted('TICKET_EDIT',object)"
 *          },
 *     "DELETE" = {
 *              "security"="is_granted('TICKET_DELETE',object)"
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass=TicketRepository::class)
 */
class Ticket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idUser;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateTicket;

    /**
     * @ORM\Column(type="integer")
     */
    private $idStatus;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="ticket", cascade={"remove"})
     */
    private $messages;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?int
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDateTicket(): ?\DateTimeInterface
    {
        return $this->dateTicket;
    }

    public function setDateTicket(\DateTimeInterface $dateTicket): self
    {
        $this->dateTicket = $dateTicket;

        return $this;
    }

    public function getIdStatus(): ?bool
    {
        return $this->idStatus;
    }

    public function setIdStatus(bool $idStatus): self
    {
        $this->idStatus = $idStatus;

        return $this;
    }

    public function addMessage(Message $message): self
    {
        $this->messages[] = $message;
        $message->setTicket($this);
        return $this;
    }

}
