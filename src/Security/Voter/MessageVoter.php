<?php

namespace App\Security\Voter;

use App\Entity\Message;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;


class MessageVoter extends Voter
{
    private $JWTEncoder;
    public function __construct(JWTEncoderInterface $JWTEncoder)
    {
        $this->JWTEncoder = $JWTEncoder;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['TICKET_EDIT', 'TICKET_DELETE'])
            && $subject instanceof \App\Entity\Message;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $idUser = $this->JWTEncoder->decode($token->getCredentials())["id"];
        if($idUser != $subject->getIdUser())
            return false;
        // if the user is anonymous, do not grant access

        // ... (check conditions and return true to grant permission) ...
//        switch ($attribute) {
//            case 'POST_EDIT':
//                // logic to determine if the user can EDIT
//                // return true or false
//                break;
//            case 'POST_VIEW':
//                // logic to determine if the user can VIEW
//                // return true or false
//                break;
//        }

        return true;
    }
}
